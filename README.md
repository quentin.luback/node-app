Cloner le projet :
git clone https://gitlab.com/quentin.luback/node-app.git

Lancer le serveur sur Heroku
heroku login
git add .
git commit -m "Envoi sur serveur"
git push heroku master

Exécuter un test avec Ava :

Tapez npm test
	Si la console affiche "1 test passed", le formulaire existe bien dans la page HTML
	Si la console affiche "1 test failed", le formulaire n'existe pas



Ré-écrire ce code de manière à ce qu'il utilise async et await, au lieu de then() et catch(). Les erreurs doivent être correctement interceptées.

const MongoClient = require('mongodb').MongoClient;
MongoClient.connect('mongodb://localhost:27017/myapp')
  .then((client) => client.db('myapp').collection('dates').find().toArray())
  .then((dates) => console.log('dates:', dates))
  .catch((err) => console.error('erreur:', err));
  

(async () => {
  try {
    const MongoClient = require('mongodb').MongoClient;
    const client = new MongoClient(url, { useNewUrlParser: true });
    await client.connect();
    const dates = await client.db('myapp').collection('dates').find().toArray();
    console.log('dates:', dates);
  }
  catch(err) {
    console.error('erreur:', err)
  }
})();
  
  
  
Le fichier server.js contient le code suivant:

const express = require('express');
const app = express();
Quelles lignes de code faut-il ajouter à ce fichier pour que:

curl -X POST http://localhost:3000/hello réponde "Missing country" (toujours au format texte brut, et sans les guillemets) avec un code 400 de status HTTP,
curl -X POST http://localhost:3000/hello?country=Zimbabwe réponde "Hello, Zimbabwe!" (au format texte brut, sans les guillemets, et le nom du pays devra systématiquement correspondre à celui passé en paramètre),
... une fois qu'on aura exécuté ce programme avec node server.js ?

Respecter les chaines de caractères fournies à la lettre.

app.post('/hello', (req,res) => {7
  if(req.query.country) {
    res.send('Hello, ' + req.query.country + '!');
  } else {
    res.status(400).send('Missing country');
  }
});



L'objectif est d'afficher dans la sortie standard (c.a.d. en utilisant console.log()) le nom de plusieurs personnes dont les données seront à récupérer en JSON, depuis des URLs listées dans un tableau JavaScript. Le nom est fourni via la propriété name de la réponse à ces requêtes.

Pour cela, nous allons compléter le programme Node.js suivant:

const https = require('https');
const urlsToFetch = [
 'https://js-jsonplaceholder.herokuapp.com/users/1',
 'https://js-jsonplaceholder.herokuapp.com/users/2',
 'https://js-jsonplaceholder.herokuapp.com/users/3'
];
Consignes à respecter:

Seul le nom des personnes doit être affiché, sans préfixe et à raison d'une par ligne.
L'affichage de ces noms doit respecter l'ordre de leurs URLs respectives dans le tableau urlsToFetch.
Votre programme devra utiliser le module https fourni par Node.js pour effectuer les requêtes. Aucune autre dépendance ne pourra être utilisée.
En cas d'erreur lors d'une requête, afficher "Error." (sans les guillemets) au lieu du nom dont la récupération a échoué.
Enfin, les URLs fournies dans urlsToFetch, leur ordre, ainsi que leur nombre peuvent changer. Le programme doit donc fonctionner en s'adaptant au contenu de ce tableau.
Fournir les lignes de code à ajouter au programme fourni ci-dessus de manière à ce qu'il affiche les noms quand on l'exécutera avec node.

for(var i=0; i < urlsToFetch.length; i++) {
  (async () => {
    await https.get(urlsToFetch[i], function(res){
      var body = '';
    
      res.on('data', function(chunk){
          body += chunk;
      });
    
      res.on('end', function(){
          var userData = JSON.parse(body);
          console.log(userData.name);
      });
    }).on('error', function(e){
        console.log("Error.");
    });
  })();
}


Déployer en production (sur Heroku) un serveur Web en Node.js mettant à disposition les routes suivantes:

POST / retourne le texte "Bonjour !" (sans les guillemets)
POST /text retourne le texte "test" (sans les guillemets)
Au lieu de fournir le code JavaScript de ce serveur, collez seulement l'URL Heroku de ce serveur dans le champ ci-dessous:

https://node-partiel.herokuapp.com/