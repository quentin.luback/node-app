var express = require('express');
var flash = require('connect-flash');
// var request = require('request');
const fetch = require('node-fetch');
md5 = require('js-md5');
var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy;
var session = require("express-session"),
    bodyParser = require("body-parser");
var router = express.Router();

// Passport
router.use(express.static("public"));
router.use(session({ secret: "cats" }));
router.use(bodyParser.urlencoded({ extended: false }));
router.use(passport.initialize());
router.use(passport.session());

var users = {
  'user1': { hashedPassword: 'a5dddb7705470be27a85a22a3612ed5e' },
  'user2': { hashedPassword: '249a9c0de26f21f90a79a731265281f5' }
};

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use(new LocalStrategy(
  function(username, password, done) {
    var user = users.hasOwnProperty(username);
      if (user == false) {
        console.log('Username incorrect');
        return done(null, false);
      }
      else if (users[username].hashedPassword != md5(password)) {
        console.log('MDP incorrect');
        return done(null, false);
      }
    return done(null, user);
  }
));


/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(req.user.username);
  res.render('index', { title: 'Node App', favicon: '/images/new-google-favicon-512.png', username: req.user.username });
});

/* Exo 3.2
router.post('/ville', function(req, res, next) {
  res.render('ville', { nom_ville: req.body.nom_ville, description: req.body.description });
});
*/
// https://geocode.xyz/monaco?geoit=JSON&auth=793097714536174811930x1881
router.post('/ville', function(req, res, next) {
  const https = require('https');
  var objCity = '';
  https.get('https://geocode.xyz/' + req.body.nom_ville + '?geoit=JSON&auth=793097714536174811930x1881', (resp) => {
    resp.on('data', (d) => {
      objCity += d;
    });

    resp.on('end', () => {
      var coord = JSON.parse(objCity);
      var longt = coord.longt;
      var latt = coord.latt;
      if(longt == 0 && latt == 0) {
        res.render('ville', { nom_ville: req.body.nom_ville, description: req.body.description, position: 'Aucunes coordonnées trouvées pour la ville saisie !' })
      }
      else {
        res.render('ville', { nom_ville: req.body.nom_ville, description: req.body.description, longt: longt, latt: latt, position: 'Longitude: ' + longt + ', Latitude : ' + latt })
      }
    })
  }).on('error', (e) => {
    console.error(e);
  });

});

// Exercice Sessions

router.get('/login', function(req, res, next) {
  res.render('login');
});

router.post('/login',
  passport.authenticate('local', { successRedirect: '/',
                                   failureRedirect: '/login',
                                   failureFlash: true })
);  

router.post('/signup', function(req, res, next) {
  res.send(req.user);
});

module.exports = router;

